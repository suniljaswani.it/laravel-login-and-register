/**
	 * Helper.js
	 * @copyright Tymk Softwares [All Rights Reserved]
	 */
	 
	 function covertToPaisa(amount){
	 	var result = (amount*100).toString();
	 	return _.round(parseFloat(result).toFixed(2));
	 }
