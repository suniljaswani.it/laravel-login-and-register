<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
	public $fillable = [
        'first_name','last_name','email','password','mobile','status','admin_status'
    ];

    protected $hidden = [
        'password'
    ];
}



