<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Session;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// Models
use App\Models\Admin;

class RegisterController extends Controller
{
	public function register(Request $request)
	{

		if($request->isMethod('post'))
		{
			$this->validate($request,[
				'first_name' => 'required',
				'last_name' => 'required',
				'email' => 'required|email|unique:admins,email',
				'mobile' => 'required|regex:/^[987][0-9]{9}$/|digits:10|unique:admins,mobile',
				'password' => 'required|min:6',
				're_type_password' => 'required|min:6|same:password',
			], [
				'first_name.required' => 'First Name is Required',
				'last_name.required' => 'Last Name is Required',
				'email.required' => 'Email Address is required',
				'email.email' => 'Invalid Email Address',
				'email.unique' => 'This Email Address is already exist',
				'mobile.required' => 'Mobile Number is Required',
				'mobile.unique' => 'This Mobile Number is already exist',
				'mobile.regex' => 'Invalid Mobile Number',
				'mobile.digits' => 'Mobile Number must be 10 digits',
				'password.required' => 'Password is required',
				'password.min' => 'Password length should be 6 or more',
				're_type_password.required' => 'Re-Type Password is required',
				're_type_password.min' => 'Re-Type Password length should be 6 or more',
				're_type_password.same' => 'Password and Re-Type Password Must be a same ',
			]);

			$admin = Admin::create([
				'first_name' => $request->first_name,
				'last_name' => $request->last_name,
				'email' => $request->email,
				'mobile' => $request->mobile,
				'password' => Hash::make($request->password),
				
			]);
			Session::put('user_registration', $request->all());

			if(Session::has('user_registration'))
			{
				return redirect()->route('admin-login',['admin' => $admin->first_name])->with([
					'success' => 'Hello '.$admin->first_name.', Your Registration has been successfully completed!',
				]);
			}

		}

		return view('admin.register');

	}

}
