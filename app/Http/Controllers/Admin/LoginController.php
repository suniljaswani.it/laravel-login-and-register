<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Session;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

// Models
use App\Models\Admin;


class LoginController extends Controller
{
	public function index(Request $request)
    {
        if($request->isMethod('post')){
            
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if($admin = Admin::where('email', $request->email)->where('status',1)->first()){
                
                if(Hash::check($request->password, $admin->password)){
                    Session::put('admin', $admin);
                }
            }
            
            return redirect()->back()->with('error', 'Invalid Email or Password or Rejected..!!');
        }

        if(Session::has('admin'))
            return redirect()->route('admin-dashboard');

        return view('admin.login');
    }

    public function logout()
    {
        Session::forget('admin');
        return redirect()->route('admin-login');
    }
}
