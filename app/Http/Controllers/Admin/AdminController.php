<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Hash;
use Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Admin;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.manager.view', ['admins' => Admin::get() ]);
    }

    

    public function create(Request $request)
    {
        if($request->isMethod('post')){

            $this->validate($request, [
                'name' => 'required',
                'mobile' => 'required|regex:/^[987][0-9]{9}$/|digits:10|unique:admins,mobile',
                'email' => 'required|email|unique:admins,email',
                'password' => 'required|min:6',
                'routes' => 'required'
            ],[
                'name.required' => 'Name is Required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.unique' => 'This Mobile Number is already exist',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'email.required' => 'Email Address is required',
                'email.email' => 'Invalid Email Address',
                'email.unique' => 'This Email Address is already exist',
                'password.required' => 'Email Address is required',
                'password.min' => 'Password length should be 6 or more',
                'routes.required' => 'Select Page for this Manager'
            ]);


            $admin = Admin::create([
                'name' => $request->name,
                'mobile' => $request->mobile,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

            \DB::transaction( function() use($request, $admin) {
                collect($request->routes)->map( function($route) use ($admin) {
                    AdminRole::create(['admin_id' => $admin->id, 'route' => $route]);
                });
            });

            return redirect()->route('admin-manager-view')->with(['success' => 'New Admin Manager has been created..!!']);
        }

        return view('admin.manager.create', ['routes' => $this->getRoutes()]);
    }

    public function update(Request $request, $id)
    {
        if($request->isMethod('post')){

            $admin = Admin::find($id);

            $this->validate($request, [
                'name' => 'required',
                'mobile' => 'required|regex:/^[987][0-9]{9}$/|digits:10|unique:admins,mobile,'.$admin->id,
                'email' => 'required|email|unique:admins,email,'.$admin->id,
                'password' => 'min:6',
                'routes' => 'required'
            ],[
                'name.required' => 'Name is Required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.unique' => 'This Mobile Number is already exist',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'email.required' => 'Email Address is required',
                'email.email' => 'Invalid Email Address',
                'email.unique' => 'This Email Address is already exist',
                'password.min' => 'Password length should be 6 or more',
                'routes.required' => 'Select Permission Pages for this Manager'
            ]);

            $admin->name = $request->name;
            $admin->mobile = $request->mobile;
            $admin->email = $request->email;
            $admin->status = $request->status;
            $admin->password = $request->password != "" ? Hash::make($request->password) : $admin->password;
            $admin->save();


            \DB::transaction( function() use($id) {
                AdminRole::where('admin_id', $id)->delete();
            });

            \DB::transaction( function() use($request, $admin) {
                collect($request->routes)->map( function($route) use ($admin) {
                    AdminRole::create(['admin_id' => $admin->id, 'route' => $route]);
                });
            });

            return redirect()->route('admin-manager-view')->with(['success' => 'Admin Manager has been updated..!!']);
        }

        // --- Retrive Selected Routes --- //
        $exist_routes = AdminRole::where('admin_id', $id)->select(['route'])->get();

        $routes = collect($this->getRoutes())->map( function($route) use ($exist_routes) {

            if(count($exist_routes->filter( function($exist_route) use ($route){
                    return $route['key'] == $exist_route->route;
                })) > 0)
                $route['selected'] = true;

            return $route;
        })->all();

        return view('admin.manager.update', ['admin' => $admin = Admin::find($id), 'routes' => $routes]);
    }


    public function profile(Request $request)
    {
        if($request->isMethod('post')){

            $admin = Admin::find(Session::get('admin')->id);

            $this->validate($request, [
                'name' => 'required',
                'mobile' => 'required|regex:/^[987][0-9]{9}$/|digits:10|unique:admins,mobile,'.$admin->id,
                'email' => 'required|email|unique:admins,email,'.$admin->id,
                'password' => 'min:6'
            ],[
                'name.required' => 'Name is Required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.unique' => 'This Mobile Number is already exist',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'email.required' => 'Email Address is required',
                'email.email' => 'Invalid Email Address',
                'email.unique' => 'This Email Address is already exist',
                'password.min' => 'Password length should be 6 or more',
            ]);

            $admin->name = $request->name;
            $admin->mobile = $request->mobile;
            $admin->email = $request->email;
            $admin->password = $request->password ? Hash::make($request->password) : $admin->password;
            $admin->save();

            Session::put('admin', $admin);

            return redirect()->route('admin-dashboard')->with(['success' => 'Profile has been updated..!!']);
        }

        return view('admin.profile');
    }

    // private function getRoutes()
    // {
    //     $routes = collect(\Route::getRoutes())->map( function($route){
    //         return $route->getName();
    //     })->reject( function($route){
    //         return in_array($route, AdminRole::openRoutes());
    //     })->unique()->map( function ($route){
    //         return ['key' => $route, 'value' => str_replace('admin-', '', $route), 'selected' => false];
    //     })->values()->all();

    //     $admin_routes =  collect($routes)->filter( function($route){
    //        return preg_match('/admin/', $route['key'], $matches, PREG_OFFSET_CAPTURE) == 1;
    //     })->all();

    //     return $admin_routes;
    // }
}
