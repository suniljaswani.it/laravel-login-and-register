<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
// use App\Models\UserDocument;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;



class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard',[
            'admins' => Admin::where('admin_status',0)->orderBy('id','desc')->get()
        ]);

    }

}
