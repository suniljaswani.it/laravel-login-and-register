<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function ()
{

    Route::get('/login', 'LoginController@index')->name('admin-login');
    Route::post('/login', 'LoginController@index')->name('admin-login');
    Route::get('/logout', 'LoginController@logout')->name('admin-logout');

    Route::get('/register', 'RegisterController@register')->name('admin-register');
	Route::post('/register', 'RegisterController@register')->name('admin-register');

    Route::group(['middleware' => 'adminAuth'], function(){

        Route::get('dashboard', 'DashboardController@index')->name('admin-dashboard');

        Route::get('access-denied', function () {
            return view('admin.access-denied');
        })->name('admin-access-denied');
        
    });

});