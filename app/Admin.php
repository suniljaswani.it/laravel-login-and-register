<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    public $fillable = [
        'first_name','last_name','email','password','mobile','status'
    ];

    protected $hidden = [
        'password'
    ];
}

