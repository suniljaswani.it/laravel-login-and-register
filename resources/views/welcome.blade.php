<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Laravel Login and Register Practical</div>
            </div>
            <br/>
            <div>
                <a href="/admin/login"><h3>Login</h3></a> &nbsp;
                <a href="/admin/register"><h3>Register</h3></a>
            </div>
            <br/> 
            <div>
                <p><b>Login with email = admin@gmail.com | password = 123456</b></p>
            </div>
        </div>
    </body>
</html>
