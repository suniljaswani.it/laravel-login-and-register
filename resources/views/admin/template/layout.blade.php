<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Admin Panel">
    <link rel="shortcut icon" href="/admin-assets/images/favicon.ico">
    <title>@yield('title') - Sunil Jaswani</title>
    <!-- App css -->
    @yield('import-css')

    <link href="/admin-assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="/admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/admin-assets/plugins/switchery/switchery.min.css">
    <script src="/admin-assets/js/modernizr.min.js"></script>
    @yield('page-css')

</head>


<body class="fixed-left">
    <div id="wrapper">
        @include('admin.template.header')
        @include('admin.template.sidemenu')
        <div class="content-page">
            <div class="content">
                <div class="container">
                    @yield('content')
                </div>
            </div>
            <footer class="footer text-right"> {{ date('Y') }} © Sunil Jaswani. </footer>
        </div>
        @include('admin.template.right-sidemenu')
    </div>

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="/admin-assets/js/jquery.min.js"></script>
    <script src="/admin-assets/js/bootstrap.min.js"></script>
    <script src="/admin-assets/js/detect.js"></script>
    <script src="/admin-assets/js/fastclick.js"></script>
    <script src="/admin-assets/js/jquery.blockUI.js"></script>
    <script src="/admin-assets/js/waves.js"></script>
    <script src="/admin-assets/js/jquery.slimscroll.js"></script>
    <script src="/admin-assets/js/jquery.scrollTo.min.js"></script>
    <script src="/admin-assets/plugins/switchery/switchery.min.js"></script>


    <script src="/admin-assets/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="/admin-assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
    <script src="/admin-assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>

    <!-- App js -->
    <script src="/admin-assets/js/jquery.core.js"></script>
    <script src="/admin-assets/js/jquery.app.js"></script>
    <script src="/admin-assets/js/moment.js"></script>
    <script src="/admin-assets/js/lodash.js"></script>
    <script src="/admin-assets/js/helper.js"></script>


    @yield('import-javascript')

    @yield('page-javascript')
</body>
</html>