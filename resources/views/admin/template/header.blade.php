<div class="topbar">
	<div class="topbar-left">
		<a href="/admin" class="logo">
            {{-- <img src="" alt="" style="height: 80%; background-color: #FFFFFF;padding: 3px;border-radius: 5px;"> --}}
            <span>Master<span> Admin</span></span>
            <i class="mdi mdi-layers"></i>
        </a>
	</div>
	<div class="navbar navbar-default" role="navigation">

		<div class="container">
			<ul class="nav navbar-nav navbar-left">
                <li>
                    <button class="button-menu-mobile open-left waves-effect">
                        <i class="mdi mdi-menu"></i>
                    </button>
                </li>
               
            </ul>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown user-box">
                    <a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                        <img src="/admin-assets/images/favicon.ico" alt="user-img" class="img-circle user-img">
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                        <li>
                            <h5>Hi, {{ Session::get('admin')->first_name }}</h5>
                        </li>
                        <li><a href="{{ route('admin-logout') }}"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li> 
                    </ul>
                </li>

			</ul>
		</div>
	</div>
</div>