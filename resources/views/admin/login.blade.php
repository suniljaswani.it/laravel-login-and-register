<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- App favicon -->
    <link rel="shortcut icon" href="/admin-assets/images/favicon.ico">
    <!-- App title -->
    <title>Login to Account</title>
    <!-- App css -->
    <link href="/admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="/admin-assets/css/style.css" rel="stylesheet" type="text/css" />
    <style>
        .bg-theme {
            background: -webkit-gradient(linear, left top, right top, color-stop(0%, #2d2f78), color-stop(100%, #00aae6));
            background: -webkit-linear-gradient(left, #2d2f78 0%,#00aae6 100%);
            background: -o-linear-gradient(left, #2d2f78 0%, #00aae6 100%);
            background: -ms-linear-gradient(left, #2d2f78 0%, #00aae6 100%);
            background: linear-gradient(to right, #2d2f78 0%, #00aae6 100%);
        }
    </style>

    <script src="/admin-assets/js/modernizr.min.js"></script>
</head>


<body class="bg-grey">

    <!-- HOME -->
    <section>
        <div class="container-alt">
            <div class="row">
                <div class="col-sm-12">
                    <div class="wrapper-page">
                        <div class="m-t-40 account-pages">
                         @if (session('error'))
                             <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if (session('success'))
                            <div class="alert alert-success"> {{ session('success') }}</div>
                        @endif
                        <div class="text-center account-logo-box bg-theme">
                            <h2 class="text-uppercase">
                                <a href="/" class="text-success">
                                    <h3 style="color:#FFF;">Login</h3>
                                    {{-- <span><img src="/admin-assets/images/logo.png" alt="" height="36"></span> --}}
                                </a>
                            </h2>
                        </div>
                        <div class="account-content bg-white">
                            <form class="form-horizontal" action="" method="post">
                                {{ csrf_field() }}
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="email" name="email" required="" placeholder="Email">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="password" name="password" required="" placeholder="Password">
                                    </div>
                                </div>

                                <div class="form-group account-btn text-center m-t-10">
                                    <div class="col-xs-12">
                                        <button class="btn w-md btn-bordered btn-primary waves-effect waves-light" type="submit">Log In</button>
                                    </div>
                                </div>

                            </form>

                            <div class="clearfix"></div>

                        </div>
                    </div>
                    <!-- end card-box-->
                </div>

            </div>
        </div>
    </div>
</section>
<!-- END HOME -->

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="/admin-assets/js/jquery.min.js"></script>
<script src="/admin-assets/js/bootstrap.min.js"></script>
<script src="/admin-assets/js/detect.js"></script>
<script src="/admin-assets/js/fastclick.js"></script>
<script src="/admin-assets/js/jquery.blockUI.js"></script>
<script src="/admin-assets/js/waves.js"></script>
<script src="/admin-assets/js/jquery.slimscroll.js"></script>
<script src="/admin-assets/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<script src="/admin-assets/js/jquery.core.js"></script>
<script src="/admin-assets/js/jquery.app.js"></script>

</body>
</html>