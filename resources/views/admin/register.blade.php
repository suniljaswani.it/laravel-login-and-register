<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- App favicon -->
    <link rel="shortcut icon" href="/admin-assets/images/favicon.ico">
    <!-- App title -->
    <title>Register</title>
    
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
        
    <style>
        .note
        {
            text-align: center;
            height: 80px;
            background: -webkit-linear-gradient(left, #0072ff, #8811c5);
            color: #fff;
            font-weight: bold;
            line-height: 80px;
        }
        .form-content
        {
            padding: 5%;
            border: 1px solid #ced4da;
            margin-bottom: 2%;
        }
        .form-control{
            border-radius:1.5rem;
        }
        .btnSubmit
        {
            border:none;
            border-radius:1.5rem;
            padding: 1%;
            width: 20%;
            cursor: pointer;
            background: #0062cc;
            color: #fff;
        }
        .spacetb-10{
            margin: 10px 0;
        }
        .spacetb-20{
            margin: 20px 0;
        }
    </style>
</head>

<body>

    <div class="container register-form">
        
        <div class="form">
            <div class="note">
                <p>Client Registration</p>
            </div>
            <div class="text-center spacetb-20">
                @if (session('errors'))
                    <div class="alert alert-danger">
                        @foreach (session('errors')->all() as $error)
                            {{ $error }}
                        @endforeach
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
               
            </div>
            <form class="form-horizontal m-t-20" method="post">
            {{ csrf_field() }}
                <div class="form-content">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="first_name" class="form-control" placeholder="Your First Name *" value="{{ old('first_name') }}" required/>
                            </div>
                            <div class="form-group">
                                <input type="text" name="last_name" class="form-control" placeholder="Your Last Name *" value="{{ old('last_name') }}" required/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Your Email *" value="{{ old('email') }}" required/>
                            </div>
                            <div class="form-group">
                                <input type="text" name="mobile" value="{{ old('mobile') }}" class="form-control" placeholder="Mobile*" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Your Password *" value="" required/>
                            </div>
                            <div class="form-group">
                                <input type="re_type_password" name="re_type_password" class="form-control" placeholder="Confirm Password *" value="" required/>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btnSubmit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
