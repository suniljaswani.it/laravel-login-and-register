@extends('admin.template.layout')

@section('title', 'Access Denied')

@section('content')
<div class="row">
	<div class="col-sm-12 text-center">

		<div class="wrapper-page">
		<img src="/admin-assets/images/animat-rocket-color.gif" alt="" height="120">
			<h1 style="font-size: 78px;">Oops..!!</h1>
			<h3 class="text-uppercase text-danger">Access Denied..!!</h3>
			<p class="text-muted">You are not allow to visit this page..!!</p>

			<a class="btn btn-success waves-effect waves-light m-t-20" href="{{ route('admin-dashboard') }}"> Return Home</a>
		</div>

	</div>
</div>
@stop