@extends('admin.template.layout')

@section('title', 'Dashboard')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Dashboard</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li class="active"> Dashboard </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            @if (session('success'))
            <div class="alert alert-success"> {{ session('success') }}</div>
            @endif
        </div>
    </div>

    {{-- Boxes --}}
    @if(Session::get('admin')->admin_status == 1)
        <div class="row">
            <div class="card-box">
                <h3>List of Clients</h3>
                <div class="table-responsive" id="UserDetails">
                    <table class="table table-bordered table-hover" >
                        <thead>
                        <tr>
                            <th> Name </th>
                            <th> Email </th>
                            <th> Phone </th>
                            <th> Status </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($admins as $admin)
                            <tr>
                                <td>{{ $admin->first_name.' '.$admin->last_name }} </td>
                                <td>{{ $admin->email }}</td>
                                <td>{{ $admin->mobile }}</td>
                                <td>
                                    @if ($admin->status == 1 )
                                        <span class="label label-success">Success</span>
                                    @else
                                        <span class="label label-danger">Rejected</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- <div class="col-md-3">
                <a href="{{ route('admin-client-list') }}">
                <div class="card-box widget-box-two widget-two-primary">
                    <i class="fa fa-users widget-two-icon"></i>
                    <div class="wigdet-two-content">
                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Clients</p>
                        <h2><span data-plugin="counterup"></span> <small><i class="mdi mdi-arrow-up text-success"></i></small></h2>
                        <p class="text-muted m-0">View More</p>
                    </div>
                </div>
                </a>
            </div>
            --}}

        </div>
    @endif

@stop

